const store = {
  players: ['player1', 'player2'],
  currentGame: {
    started: false,
    turn: 0,
    winner: null,
    pawn: 'X',
    score1: 0,
    score2: 0,
  },
  tictactoe: [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ],
};

const tictactoeDivs = Object.assign([], store.tictactoe);
for (let i = 0; i < 3; i += 1) {
  for (let j = 0; j < 3; j += 1) {
    tictactoeDivs[i][j] = document.getElementById(`${i}${j}`);
  }
}
const winnerMessageDiv = document.getElementById('winner_message');
const scoreDiv = document.getElementById('score');
const winnerDiv = document.getElementById('winner_message');
var grid = document.getElementsByClassName('grid');
var actions = document.getElementsByClassName('actions');
var hud = document.getElementsByClassName("HUD")
var menuDiv = document.getElementsByClassName("menu")
var menuPvpDiv = document.getElementsByClassName('menu_pvp');
var menuAiDiv = document.getElementsByClassName('menuai');
var menuOptionsDiv = document.getElementsByClassName('menu_options');
console.log(hud)

/// Menuing
function mainMenu(){
  menuOptionsDiv[0].style.display = 'none';
  menuPvpDiv[0].style.display = 'none';
  menuAiDiv[0].style.display ='none'
  actions[0].style.display='none'
  grid[0].style.display ='none'
  hud[0].style.display ='none'

  menuDiv[0].style ='visible'
}
function menuPVP(){
  menuDiv[0].style.display ='none'
  menuPvpDiv[0].style='visible'

  console.log(menuPvpDiv[0].style.display)
}

function menuAI(){
  menuDiv[0].style.display = 'none';
  menuAiDiv[0].style = 'visible';

}

function menuOptions(){
  menuDiv[0].style.display = 'none';
  menuOptionsDiv[0].style = 'visible';
}

function firstTo3(){
  menuOptionsDiv[0].style.display = 'none';
  menuPvpDiv[0].style.display = 'none';
  menuAiDiv[0].style.display ='none';

  actions[0].style='visible'
  grid[0].style ='visible'
  hud[0].style ='visible'

}

function firstTo5(){
  menuOptionsDiv[0].style.display = 'none';
  menuPvpDiv[0].style.display = 'none';
  menuAiDiv[0].style.display ='none';

  actions[0].style='visible'
  grid[0].style ='visible'
  hud[0].style ='visible'

}

function infinite(){
  menuOptionsDiv[0].style.display = 'none';
  menuPvpDiv[0].style.display = 'none';
  menuAiDiv[0].style.display ='none';

  actions[0].style='visible'
  grid[0].style ='visible'
  hud[0].style ='visible'


}
/// end of menuing

///game 
function startGame() {
  store.currentGame.started = true;
  store.currentGame.winner = null;
  store.currentGame.turn = 0;
  store.currentGame.pawn = 'X';
  store.tictactoe = [
    [null, null, null],
    [null, null, null],
    [null, null, null],

  ];

  for (let i = 0; i < 3; i += 1) {
    for (let j = 0; j < 3; j += 1) {
      tictactoeDivs[i][j].innerHTML = '';
    }
  }
  winnerMessageDiv.innerHTML = '';
}
function currentPlayer() {
  if (store.currentGame.turn % 2 === 0) {
    return store.players[1];
  }
  return store.players[0];
}

function checkWinner() {
  if (store.tictactoe[0][0] + store.tictactoe[0][1] + store.tictactoe[0][2] === 'XXX'
  || store.tictactoe[1][0] + store.tictactoe[1][1] + store.tictactoe[1][2] === 'XXX'
  || store.tictactoe[2][0] + store.tictactoe[2][1] + store.tictactoe[2][2] === 'XXX'
  || store.tictactoe[0][0] + store.tictactoe[1][0] + store.tictactoe[2][0] === 'XXX'
  || store.tictactoe[0][1] + store.tictactoe[1][1] + store.tictactoe[2][1] === 'XXX'
  || store.tictactoe[0][2] + store.tictactoe[1][2] + store.tictactoe[2][2] === 'XXX'
  || store.tictactoe[0][0] + store.tictactoe[1][1] + store.tictactoe[2][2] === 'XXX'
  || store.tictactoe[0][2] + store.tictactoe[1][1] + store.tictactoe[2][0] === 'XXX') {
    store.currentGame.winner = 'player1';
  } else if (store.tictactoe[0][0] + store.tictactoe[0][1] + store.tictactoe[0][2] === 'OOO'
  || store.tictactoe[1][0] + store.tictactoe[1][1] + store.tictactoe[1][2] === 'OOO'
  || store.tictactoe[2][0] + store.tictactoe[2][1] + store.tictactoe[2][2] === 'OOO'
  || store.tictactoe[0][0] + store.tictactoe[1][0] + store.tictactoe[2][0] === 'OOO'
  || store.tictactoe[0][1] + store.tictactoe[1][1] + store.tictactoe[2][1] === 'OOO'
  || store.tictactoe[0][2] + store.tictactoe[1][2] + store.tictactoe[2][2] === 'OOO'
  || store.tictactoe[0][0] + store.tictactoe[1][1] + store.tictactoe[2][2] === 'OOO'
  || store.tictactoe[0][2] + store.tictactoe[1][1] + store.tictactoe[2][0] === 'OOO') {
    store.currentGame.winner = 'player2';
  }
}

function checkGameState() {
  checkWinner();
  // / si il y a un gagnant, ou si tour =9, fin de partie
  if (store.currentGame.winner !== null || store.currentGame.turn === 9) {
    store.currentGame.started = false;
  }
}

function isPlayable(x, y) {
  if (store.tictactoe[x][y] === null && store.currentGame.started === true) {
    return true;
  }
  return false;
}

function finishTurn() {
  store.currentGame.turn += 1;

  if (store.currentGame.turn % 2 === 0) { // / tour pair ///
    store.currentGame.pawn = 'X';
    store.currentGame.currentPlayer = 'player1';
  } else { // / tour impair ///
    store.currentGame.pawn = 'O';
    store.currentGame.currentPlayer = 'player2';
  }

  checkGameState();
  if (store.currentGame.started === false) {
    if (store.currentGame.winner === 'player1') {
      winnerMessageDiv.innerHTML = 'Player 1 wins !';
      store.currentGame.score1 += 1;
      scoreDiv.innerHTML = `${store.currentGame.score1}-${store.currentGame.score2}`;
    } else if (store.currentGame.winner === 'player2') {
      winnerMessageDiv.innerHTML = 'Player 2 wins !';
      store.currentGame.score2 += 1;
      scoreDiv.innerHTML = `${store.currentGame.score1}-${store.currentGame.score2}`;
    } else {
      winnerMessageDiv.innerHTML = 'Draw !';
    }
  }
}

function play(x, y) {
  if (store.currentGame.started === false) {
    startGame();
  }

  if (isPlayable(x, y) === true) {
    winnerDiv.innerHTML = `${currentPlayer()}'s turn`;

    tictactoeDivs[x][y].innerHTML = store.currentGame.pawn;
    store.tictactoe[x][y] = store.currentGame.pawn;
    finishTurn();
  }
}
